import argparse
import json
from collections import OrderedDict

import matplotlib.pyplot as plt


class ProgressGrapher:
    def graph(self, file_name, race_name, year):
        json_dump = self.load(file_name)
        rider_progression, bib_map = self._parse_stages(json_dump)
        self.plot(rider_progression, bib_map, race_name, year)

    def load(self, file_name):
        with open('data/{}'.format(file_name), 'r') as file:
            return json.load(file)

    def sort_stage(self, stage, key):
        return sorted(stage, key=lambda rider: rider[key])

    def _parse_stages(self, stages):
        """ Parse a dict of stages to a progression-per-rider structure. """
        rider_progression = OrderedDict()

        # Optional prologue handling
        if '0' in stages.keys():
            i = 0
        else:
            i = 1

        # Parse the first stage for the bib numbers and generate an intial list of results
        first_stage = stages[str(i)]
        bib_map = self._create_bib_map(first_stage)
        for rider in bib_map.keys():
            rider_progression[rider] = list()

        while True:
            stage = stages.get(str(i))
            if stage is None:
                break

            for position in stage:
                gc = position['gc']
                if gc:
                    # Set the gc to negative to reverse the plot
                    rider_progression[position['bib']].append(-gc)
            i += 1
        return rider_progression, bib_map

    def _create_bib_map(self, stage):
        """ Map bib number to rider name. """
        bib_map = dict()
        for rider in stage:
            bib_map[rider['bib']] = rider['name']
        return bib_map

    def plot(self, rider_progression, bib_map, race_name, year):
        # TODO: Extend plot with stage information
        # TODO: Extend annotations with team names, stage name?

        fig = plt.figure(figsize=(12, 9))

        for bib, progression in rider_progression.items():
            label = bib_map[bib]
            plt.plot(progression, linewidth=0.5, picker=1, alpha=0.5, label=label)

        # Hide ticks .... Very minimalistic
        plt.xticks([])
        plt.yticks([])
        axes = plt.gca()

        # Annotations and highlight magic
        annotation = axes.annotate("", xy=(0, 0), xytext=(
            20, 20), textcoords="offset points")
        annotation.set_visible(False)

        def update_annotation(pos, label, color):
            annotation.xy = pos
            annotation.set_text(label)
            annotation.set_visible(True)

        # Event handlers
        def on_click(event):
            if axes.contains(event)[0]:
                return
            else:
                # Reset highlighting if click is outside of the axis
                annotation.set_visible(False)
                for line in axes.lines:
                    line.set_alpha(0.5)
                    line.set_linewidth(0.5)
                fig.canvas.draw_idle()

        def on_pick(event):
            line = event.artist
            xdata, ydata = line.get_data()
            ind = event.ind
            pos = (xdata[ind], ydata[ind])

            update_annotation(pos, line.get_label(), line.get_color())

            for other_line in axes.lines:
                other_line.set_alpha(0.3)
                other_line.set_linewidth(0.5)

            line.set_linewidth(1)
            line.set_alpha(1)

            fig.canvas.draw_idle()

        # Register event handlers
        fig.canvas.mpl_connect('pick_event', on_pick)
        fig.canvas.mpl_connect('button_press_event', on_click)

        # TODO: Petty race name
        title = '{} {}'.format(race_name.replace('-', ' ').upper(), year)
        fig.suptitle(title)

        plt.show()


def main():
    parser = argparse.ArgumentParser(description='Plot Progress Graph')
    parser.add_argument(
        'file_name', type=str, help='File name for the json. Expects the file in data/file_name.')
    args = parser.parse_args()

    grapher = ProgressGrapher()
    grapher.graph(args.file_name)


if __name__ == '__main__':
    main()
