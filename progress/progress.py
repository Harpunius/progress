import argparse

from fetch import RaceBuilder
from graph import ProgressGrapher


def main():
    parser = argparse.ArgumentParser(description='Fetch and Plot Progress')
    parser.add_argument(
        'race_name', type=str, help=('Name of the race to fetch. Expects the name as '
                                     'it appears in the url on PCS.'))
    # TODO: Create mapper for human names -> PCS urls

    parser.add_argument(
        'year', type=str, help='Year of the race to progress.')
    args = parser.parse_args()

    builder = RaceBuilder()
    file_name = builder.build_race(race_name=args.race_name, year=args.year)

    grapher = ProgressGrapher()
    grapher.graph(file_name=file_name, race_name=args.race_name, year=args.year)


if __name__ == '__main__':
    main()
