import argparse
import json
import os.path
import requests

from bs4 import BeautifulSoup


class RaceBuilder:
    def __init__(self):
        self.parser = 'lxml'
        fake_header = ('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                       'Chrome/50.0.2661.75 Safari/537.36')
        self.headers = {'User-Agent': fake_header, 'X-Requested-With': 'XMLHttpRequest'}
        self.base_url = 'https://www.procyclingstats.com/'
        self.invalid_gc_tables = ['today', 'bonifications']

    def build_race(self, race_name=None, year=None):
        file_name = '{}{}.json'.format(race_name, year)
        
        if self._race_already_processed(file_name):
            # TODO: Flag to force re-processing
            return file_name

        race = Race(race_name, year)

        url = 'race/{}/{}'.format(race_name, year)
        stages = self._fetch_stages(url)

        for stage in stages:
            rows, indices = self._fetch_rows(stage.url)
            self._parse_rows_into_stage(rows, stage, indices)
            race.add_stage(stage)
        
        self._dump(race, file_name)
        return file_name

    def _race_already_processed(self, file_name):
        return os.path.isfile('data/{}'.format(file_name)) 

    def _order_soup(self, url):
        response = requests.get(url, headers=self.headers)
        return BeautifulSoup(response.content, self.parser)

    def _fetch_stages(self, url):
        soup = self._order_soup(self.base_url + url)

        navs = soup.find_all(class_='pageSelectNav')
        nav = navs[1]
        options = nav.find_all('option')

        def option_to_stage(option, is_prologue=False):
            option_text = option.text
            stage_name = option_text

            # Transform race/x/y/stage-1/result/result to race/x/y/stage-1-gc
            stage_url = option['value'].replace('/result/result', '-gc')
            
            if is_prologue:
                # Prologues are treated as stage 0
                stage_number = 0
            else:
                stage_number = option_text.split()[1]

            return Stage(stage_number, stage_name, stage_url)

        stages = list()
        for i in range(len(options)):
            option = options[i]
            if 'Prologue' in option.text:
                stages.append(option_to_stage(option, is_prologue=True))
            elif 'Stage' in option.text:
                stages.append(option_to_stage(option))
            else:
                # Ignore KOM/Points/Youth etc
                pass

        # Url of last stage is different (race/x/y/gc)
        last_stage = stages[-1]
        last_stage.url = last_stage.url[:last_stage.url.find('stage')] + 'gc'

        return stages

    def _fetch_rows(self, url):
        """
        Heuristic best-effort parsing that will break whenever the html changes
        """
        soup = self._order_soup(self.base_url + url)

        gc_soup = None
        for candidate in soup.findAll(class_='result-cont'):
            if 'hide' in candidate['class']:
                continue
            gc_soup = candidate
            break

        # Columns are not consistent, try to parse the indices
        indices = self._parse_indices(gc_soup.find('thead'))
        return gc_soup.find('tbody').find_all('tr'), indices

    def _parse_indices(self, header_soup):
        headers = header_soup.find_all('th')
        gc_index = headers.index(header_soup.find('th', text='Rnk'))
        bib_index = headers.index(header_soup.find('th', text='BIB'))
        name_index = headers.index(header_soup.find('th', text='Rider'))
        team_index = headers.index(header_soup.find('th', text='Team'))

        return Indices(gc_index, bib_index, name_index, team_index)

    def _parse_rows_into_stage(self, rows, stage, indices):
        gc_index = indices.gc
        bib_index = indices.bib
        name_index = indices.name
        team_index = indices.team

        for row in rows:
            contents = row.contents

            try:
                n = contents[name_index].find('a')
                name = n.text

                # TODO: Re-enable doper handling..?
                # Damned be the dopers (skip them)
                #if name_tag.find('s'):
                #    print('Skipping doper')
                #    continue

                gc = contents[gc_index].text.strip()
                if gc in ('DNF', 'DNS', 'OTL', ''):
                    continue
                gc = int(gc)
                bib = int(contents[bib_index].text.strip())

                team = contents[team_index].text.strip()

                result = Result(gc, bib, name, team)
                stage.add_result(result)
            except IndexError as e:
                print(e)
                exit()

        return stage

    @staticmethod
    def _dump(race, file_name):
        with open('data/{}'.format(file_name), 'w') as file:
            json.dump(race, file, cls=RaceJSONEncoder)


class Indices:
    def __init__(self, gc=0, bib=1, name=2, team=4):
        self.gc = gc
        self.bib = bib
        self.name = name
        self.team = team

    def __repr__(self):
        return 'Indices gc: {}, bib: {}, name: {}, team: {}'.format(self.gc, self.bib, self.name, self.team)


class RaceJSONEncoder(json.JSONEncoder):
    @staticmethod
    def default(obj):
        if isinstance(obj, (Result, Stage, Race)):
            return obj.serializable()
        return super().default(obj)


class Race:
    def __init__(self, name, year):
        self.stages = dict()
        self.name = name
        self.year = year

    def __repr__(self):
        return 'Race {} ({}): {} stage(s)'.format(self.name, self.year, len(self.stages))

    def add_stage(self, stage):
        self.stages[stage.stage_number] = stage.results

    def serializable(self):
        return self.stages


class Stage:
    def __init__(self, stage_number, name, url):
        self.stage_number = stage_number
        self.name = name
        self.url = url
        self.results = list()

    def __repr__(self):
        return 'Stage {}: {} result(s)'.format(self.stage_number, len(self.results))

    def add_result(self, result):
        self.results.append(result)

    def serializable(self):
        return {self.stage_number: self.results}


class Result:
    def __init__(self, gc, bib, name, team):
        self.gc = gc
        self.bib = bib
        self.name = name
        self.team = team

    def __repr__(self):
        return str(self.serializable())

    def serializable(self):
        return dict(gc=self.gc, bib=self.bib, name=self.name, team=self.team)


def main():
    parser = argparse.ArgumentParser(description='Fetch Progress')
    parser.add_argument(
        'race_name', type=str, help=('Name of the race to fetch. Expects the name as '
                                     'it appears in the url on PCS.'))

    parser.add_argument(
        'year', type=str, help='Year of the race to progress.')
    args = parser.parse_args()

    builder = RaceBuilder()
    builder.build_race(race_name=args.race_name, year=args.year)


if __name__ == '__main__':
    main()
